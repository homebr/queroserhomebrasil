# Quero ser Homebrasil!

Bom, primeiro você tem que saber que queremos no nosso time gente que **ama** a profissão: que ama programar, resolver problemas complexos, conhecer novas tecnologias, frameworks, linguagens e padrões.

Agora, vamos lá:

### Como devo me candidatar?

Basta criar um fork desse repositório e...

* Escolher dois dos três testes disponíveis no diretório testes.
* Resolva-os utilizando PHP, fique a vontade para utilizar os pacotes que achar necessário.
* Abra um pull request informando o seu e-mail de contato :)
