Dentro do arquivo data.csv você encontrará uma lista com diversos pedidos de músicas.
Você deve escrever um programa que irá ler essa lista e imprimir uma lista com as **bandas** e o **número de músicas pedidas desta banda** (músicas repetidas devem ser desconsideradas) ordenadas em ordem decrescente.

**Exemplo de Entrada:**

	Música;Banda;Ouvinte;
	Anunciação;Alceu Valença;Edson;
	Rehab;Amy Winehouse;Edson;
	Sad but true;Metallica;Tiago;
	Anunciação;Alceu Valença;Batman;	

**Exemplo de Saída:**

	Alceu Valença 2
	Amy Winehouse 1
	Metallica 1