Dentro do arquivo users.csv você encontrará uma lista com diversos usuários.
Sua tarefa é construir uma aplicação utilizando ZF2 que exiba um formuĺário de "login" e procure o usuário nessa planilha. Caso o usuário seja encontrado a aplicação deverá exibir uma tabela com o nome de todos os usuários existentes no csv, caso contrário deverá exibir uma tela de acesso negado.

**Observações:**

	* O formulário deverá possuir os campos e-mail e senha.
	* Fique a vontade para utilizar os pacotes que desejar.
	* O acesso a "tabela de usuários" só pode ser realizado por usuários autenticados.
