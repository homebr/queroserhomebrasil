Dentro do arquivo users.csv você encontrará uma lista com diversos usuários.
Sua tarefa é construir uma aplicação utilizando Codeigniter que exiba um formuĺário de "login" e procure o usuário nessa planilha. Caso o usuário seja encontrado a aplicação deverá exibir o usuário um menu com dois links (pagina-a e pagina-b), caso contrário deverá exibir uma tela de acesso negado.

**Observações:**

	* O formulário deverá possuir os campos e-mail e senha.
	* Fique a vontade para utilizar os pacotes que desejar.
	* O acesso as páginas (pagina-a e pagina-b) só pode ser realizado por usuários autenticados.
	* O conteúdo das páginas (pagina-a e pagina-b) são irrelevantes, fique a vontade para colocar um lorem ipsum.
